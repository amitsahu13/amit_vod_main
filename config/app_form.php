<?php
return [
    'inputContainer' => '<div class="input-group {{type}}{{required}}">{{content}}</div>',
    'inputContainerError' => '<div class="input input-group {{type}}{{required}} error">{{content}}{{error}}</div>',
];