<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity.
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'firstname' => true,
        'lastname' => true,
        'dob' => true,
        'mob_no' => true,
        'username' => true,
        'password' => true,
        'email' => true,
        'role' => true,
        'fb_token' => true,
        'address' => true,
    ];
    
}
