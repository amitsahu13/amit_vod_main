<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * VimeoUser Entity.
 */
class VimeoUser extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'cid' => true,
        'client_secret' => true,
        'client_access_token' => true,
    ];
}
