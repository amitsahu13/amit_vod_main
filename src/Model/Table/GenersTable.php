<?php
namespace App\Model\Table;

use App\Model\Entity\Gener;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Geners Model
 *
 */
class GenersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $c  onfig The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('geners');
        $this->displayField('name');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table','message'=>'Genre Name should be unique'])
            ->allowEmpty('name');
            
        $validator
            ->add('created_date', 'valid', ['rule' => 'datetime'])
            ->allowEmpty('created_date');
            
        $validator
            ->allowEmpty('updated_date');

        return $validator;
    }
}
