<div class="row">
     <?php echo $this->Flash->render(); ?>
    <div class="col-md-12">
        
        <div class="col-md-offset-11">
        <div class="dropdown">
            <button class="btn btn-warning dropdown-toggle " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <?= __('Actions') ?>
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="margin-left:-76px;">
              <li><?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-fw fa-backward', 'title' => 'Back']) . " Back to Genre", ['action' => 'genreindex'],['escape' => false]) ?></li>
            </ul>
        </div>
     </div> 
        <div class="form-group">
            <fieldset>
                <?= $this->Form->create($gener) ?>
                <fieldset>
                    <legend><?= __('Add Gener') ?></legend> 
                    <?php
                    echo '<div class="col-md-6">';
                    echo $this->Form->input('name', ['label' => 'Genre Name', 'required', 'class' => 'form-control']);
                    echo '<div style="display:none;">';
                    echo $this->Form->input('created_date');
                    echo $this->Form->input('updated_date');
                    echo '</div>';
                    echo $this->Form->button('<i class="fa fa-fw fa-refresh"></i>Reset', ['type' => 'reset', 'class' => 'btnmargin btnmrleft btn btn-warning pull-right', 'div' => false]);
                    echo $this->Form->button('<i class="fa fa-fw fa-floppy-o"></i>Save', ['type' => 'submit', 'class' => 'btnmargin btn btn-success pull-right', 'div' => false]);
                    echo '</div>';
                    ?>
                </fieldset>
                <?= $this->Form->end() ?>
            </fieldset> 
        </div>
    </div>
</div>