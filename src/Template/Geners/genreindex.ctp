<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Gener'), ['action' => 'add']) ?></li>
    </ul>
</div>
 <?php echo $this->Flash->render(); ?>
<div class="geners index large-10 medium-9 columns">
    <span class="pull-right"><?php echo $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-fw fa-file-o', 'title' => 'Add New API']) . "", ['controller' => 'geners', 'action' => 'genreadd'], ['escape' => false, 'class' => 'btn btn-info btn-sm']); ?></span>
     <table class="table table-bordered" id="vimeoapitable">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('created_date') ?></th>
            <th><?= $this->Paginator->sort('updated_date') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($geners as $gener): ?>
        <tr>
            <td><?= $this->Number->format($gener->id) ?></td>  
            <td><?= h($gener->name) ?></td>
            <td><?= h($gener->created_date) ?></td>
            <td><?= h($gener->updated_date) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $gener->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $gener->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $gener->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gener->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
