<div class="center-vertical">
    <div class="center-content">
        <div class="col-md-3 center-margin">
            <button class="btn btn-primary btn-sm" onclick="login()"> Sign in with facebook</button>
             <?php echo $this->Flash->render(); ?>
            <?= $this->Form->create('Login') ?>
            <div class="content-box wow bounceInDown modal-content">
                <h3 class="content-box-header content-box-header-alt bg-default">
                    <span class="icon-separator">
                        <i class="glyph-icon icon-cog"></i>
                    </span>
                    <span class="header-wrapper">
                        Members area
                        <small>Login to your account.</small>
                    </span>
                    <span class="header-buttons">
                        <?php echo $this->Html->link('Sign up' ,['controller'=>'Users','action'=>'signup'],['class'=>'btn btn-sm btn-primary']); ?>
                    </span>
                </h3>
                <div class="content-box-wrapper">
                    <div class="form-group">
                        <div class="input-group">
                            <?php echo $this->Form->password('username', ['type'=>'text','div' => false, 'label' => false, 'class' => 'form-control', 'placeholder' => 'Please Enter username']); ?>
                            <span class="input-group-addon bg-blue">
                                <i class="glyph-icon icon-envelope-o"></i>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            
                            <?php echo $this->Form->password('password', ['div' => false, 'label' => false, 'class' => 'form-control', 'placeholder' => 'Please Enter password']); ?>
                            <span class="input-group-addon bg-blue">
                                <i class="glyph-icon icon-unlock-alt"></i>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <a href="#" title="Recover password">Forgot Your Password?</a>
                    </div>
                    <?= $this->Form->button(__('Sign In'), ['class' => 'btn btn-success btn-block']) ?>
                    <!--<button class="btn btn-success btn-block">Sign In</button>-->
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>   

