<?php echo $this->Flash->render(); ?>   
<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            Dashboard
        </h2>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Dashboard
            </li>
        </ol>
    </div>
</div>
<?php if ($this->request->Session()->read('userdata')->role == 'Super Admin'): ?>
    <?= $this->element('dashboard/superadmindashboard'); ?>
<?php elseif ($this->request->Session()->read('userdata')->role == 'Admin'): ?>
    <?= $this->element('dashboard/admindashboard'); ?>
<?php elseif ($this->request->Session()->read('userdata')->role == 'Creator'): ?>
    <?= $this->element('dashboard/creatordashboard'); ?>
<?php endif; ?>