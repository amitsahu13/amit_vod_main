<?php
$firstname = "";
$lastname = "";
$email = "";
$emailvalid = "";
$username = "";
$pass = "";
$cpass = "";
$plength = "";
$cplength = "";
$passconfirm = "";
$userunique = "";
$regemail = "";
if (isset($response_api)) {
    if (isset($response_api->errormessage->firstname->_empty)) {
        $firstname = $response_api->errormessage->firstname->_empty;
    }if (isset($response_api->errormessage->lastname->_empty)) {
        $lastname = $response_api->errormessage->lastname->_empty;
    }if (isset($response_api->errormessage->email->_empty)) {
        $email = $response_api->errormessage->email->_empty;
    }if (isset($response_api->errormessage->username->_empty)) {
        $username = $response_api->errormessage->username->_empty;
    }if (isset($response_api->errormessage->password->_empty)) {
        $pass = $response_api->errormessage->password->_empty;
    }if (isset($response_api->errormessage->cpassword->_empty)) {
        $cpass = $response_api->errormessage->cpassword->_empty;
    }if (isset($response_api->errormessage->email->valid)) {
        $emailvalid = $response_api->errormessage->email->valid;
    }if (isset($response_api->errormessage->password->length)) {
        $plength = $response_api->errormessage->password->length;
    }if (isset($response_api->errormessage->cpassword->length)) {
        $cplength = $response_api->errormessage->cpassword->length;
    }if (isset($response_api->errormessage->cpassword->passwordsEqual)) {
        $passconfirm = $response_api->errormessage->cpassword->passwordsEqual;
    }if (isset($response_api->errormessage->username->unique)) {
        $userunique = $response_api->errormessage->username->unique;
    }if (isset($response_api->errormessage->email->unique)) {
        $regemail = $response_api->errormessage->email->unique;
    }
}
?>
<div class="center-vertical">
    <div class="center-content">
        <div class="col-md-4 center-margin">
            <button class="btn btn-primary btn-sm" onclick="login()"> Sign Up with facebook</button>
<?= $this->Form->create('signup') ?>
            <div class="content-box wow bounceInDown modal-content">
                <h3 class="content-box-header content-box-header-alt bg-default">
                    <span class="icon-separator">
                        <i class="glyph-icon icon-cog"></i>
                    </span>
                    <span class="header-wrapper">
                        Creator area
                        <small>Register to your account.</small>
                    </span>
                    <span class="header-buttons">
<?php echo $this->Html->link('Sign In', ['controller' => 'Users', 'action' => 'login'], ['class' => 'btn btn-sm btn-primary']); ?>

                    </span>
                </h3>
                <div class="content-box-wrapper">
                    <div class="form-group">
                        <div class="input-group">
<?php
echo $this->Form->hidden('role', ['class' => 'form-control', 'label' => false, 'value' => 'Creator']);
echo $this->Form->hidden('countries_id', ['class' => 'form-control', 'label' => false, 'value' => '1']);
echo $this->Form->hidden('cities_id', ['class' => 'form-control', 'label' => false, 'value' => '1']);
?> 
                            <?php
                            echo $this->Form->input('firstname', ['class' => 'form-control', 'label' => false, 'placeholder' => 'Enter a Firstname']);
                            ?> 
<!--                             <span class="input-group-addon bg-blue">
                                <i class="glyph-icon icon-cog"></i>
                            </span>-->
<?= $firstname; ?>
                        </div>
                    </div>     
                    <div class="form-group">
                        <div class="input-group">
<?php
echo $this->Form->input('lastname', ['class' => 'form-control', 'label' => false, 'placeholder' => 'Enter a lastname']);
?> 
<!--                             <span class="input-group-addon bg-blue">
                                <i class="glyph-icon icon-cog"></i>
                            </span>-->
<?= $lastname; ?>
                        </div>
                    </div>
                    <div class="form-group">

<?php
echo $this->Form->input('email', ['class' => 'form-control', 'label' => false, 'placeholder' => 'Enter a email']);
?> 
<!--                             <span class="input-group-addon bg-blue">
         <i class="glyph-icon icon-envelope-o"></i>
     </span>-->
<?php echo $email;
echo $emailvalid;
echo $regemail; ?>    
                    </div>
               
                <div class="form-group">
                    <div class="input-group">
                        <?php
                        echo $this->Form->input('username', ['class' => 'form-control', 'label' => false, 'placeholder' => 'Enter a username']);
                        ?> 
<!--                             <span class="input-group-addon bg-blue">
                            <i class="glyph-icon icon-user"></i>
                        </span>-->
                        <?= $username ?>  <?= $userunique ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <?php
                        echo $this->Form->input('password', ['class' => 'form-control', 'label' => false, 'placeholder' => 'Enter a password']);
                        ?> 
<!--                             <span class="input-group-addon bg-blue">
                            <i class="glyph-icon icon-unlock-alt"></i>
                        </span>-->
                        <?php echo $pass;
                        echo $plength; ?>
                    </div>
                </div> 
                <div class="form-group">
                    <div class="input-group">
                        <?php
                        echo $this->Form->password('cpassword', ['class' => 'form-control', 'label' => false, 'placeholder' => 'Retype password']);
                        ?> 
<!--                             <span class="input-group-addon bg-blue">
                            <i class="glyph-icon icon-unlock-alt"></i>
                        </span>-->
                        <?php echo $cpass;
                        echo $cplength;
                        echo'<br>';
                        echo $passconfirm; ?>
                    </div>
                </div> 
<?= $this->Form->button(__('Register'), ['class' => 'btn btn-success btn-block']) ?>
<?= $this->Form->end() ?>

            </div>
        </div>
    </div>
</div> 
