<link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">
<div class="actions columns large-2 medium-3">
    <?=
    $this->Form->postLink(
            __('Delete'), ['action' => 'delete', $video->id], ['class' => 'btn btn-default', 'confirm' => __('Are you sure you want to delete # {0}?', $video->id)]
    )
    ?>
    <?= $this->Html->link(__('List Videos'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
</div>
<div class="videos form large-10 medium-9 columns">
    <?= $this->Form->create(isset($video) ? $video : '') ?>
    <fieldset>
        <legend><?= __('Edit Video') ?></legend>
        <?php
        echo "<div class='col-md-6'>";
        echo $this->Form->hidden('id', ['value' => $id]);
        echo $this->Form->input('genres', ['id' => 'gener']);
        echo $this->Form->input('topics', ['id' => 'topic']);
        echo $this->Form->input('tags', ['id' => 'tags']);
        echo $this->Form->input('countryid');
        echo $this->Form->input('video_title');
        echo $this->Form->input('video_year');
        echo $this->Form->input('video_description');
        echo $this->Form->input('video_director');
        echo $this->Form->input('video_synopsis');
        echo "</div>";
        echo "<div class='col-md-6'>";
        echo $this->Form->input('country');
        echo $this->Form->input('film_festival');
        echo $this->Form->input('language');
        echo $this->Form->input('video_logline');
        echo $this->Form->input('video_cast');
        echo $this->Form->input('video_link');
        echo $this->Form->input('video_length');
        echo $this->Form->input('video_size');
        echo $this->Form->input('video_file_type');
        echo $this->Form->input('channel');
        echo $this->Form->input('video_trailer_url');
        echo $this->Form->input('video_twitter');
        echo "</div>";
        /* echo $this->Form->input('video_fbpage');
          echo $this->Form->input('video_referal');
          echo $this->Form->input('total_like');
          echo $this->Form->input('total_share');
          echo $this->Form->input('total_viewed');
          echo $this->Form->input('is_hd_sd');
          echo $this->Form->input('is_paid');
          echo $this->Form->input('price');
          echo $this->Form->input('is_approved');
          echo $this->Form->input('need_signup'); */
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<script>
    $(document).ready(function () {
        $('#gener').tokenfield({
            autocomplete: {
                source: ['red', 'blue', 'green', 'yellow', 'violet', 'brown', 'purple', 'black', 'white'],
                delay: 100
            },
            showAutocompleteOnFocus: true
        });
        
        $('#topic').tokenfield({
            autocomplete: {
                source: ['red', 'blue', 'green', 'yellow', 'violet', 'brown', 'purple', 'black', 'white'],
                delay: 100
            },
            showAutocompleteOnFocus: true
        });
        
        $('#tags').tokenfield({
            autocomplete: {
                source: ['red', 'blue', 'green', 'yellow', 'violet', 'brown', 'purple', 'black', 'white'],
                delay: 100
            },
            showAutocompleteOnFocus: true
        });
    });
</script>
