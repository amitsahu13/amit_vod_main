<?php //echo $this->Html->script('jquery.dataTables'); 
//echo $this->Html->Url(array('controller' => 'videos', 'action' => 'ajaxData'));
?>

<h1>Video List</h1>
 
<table id="videoList" class="responsive no-wrap table">
    <thead>
        <tr>
            <th>Video Id</th>
            <th>Video Title</th>
            <th>Video Link</th>
            <th>Action</th>
        </tr>
    </thead>
    
</table>

<script type="text/javascript">
    $(document).ready(function() {
        $('#videoList').dataTable({
            
            "bProcessing": true,
            "bServerSide": true,
            "responsive": true,
            "sAjaxSource": "<?= $this->Url->build(["controller" => "videos","action" => "ajaxData"]) ?>"
            //"sAjaxSource": "ajaxData"
        });
    });
</script>