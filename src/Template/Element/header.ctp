 <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">[ VIDEO - API ]</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
              
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Welcome <?=  ucfirst($this->request->Session()->read('userdata')->username);?><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <?php
            echo $this->Html->link(
                    $this->Html->tag('i', '', ['class' => 'fa fa-fw fa-user']) . " View Profile", ['controller' => 'Users', 'action' => 'Dashboard'], ['escape' => false]
            );
            ?>
                        </li>
                        
                        <li class="divider"></li>
                        <li>
                           <?php echo $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-fw fa-power-off']) . "Logout", ['controller' => 'Users', 'action' => 'logout'], ['escape' => false]); ?>
                        </li>
                    </ul>
                </li>
            </ul>