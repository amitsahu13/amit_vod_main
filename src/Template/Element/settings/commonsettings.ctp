<li>
            <a href="javascript:;" data-toggle="collapse" data-target="#setting"><i class="fa fa-fw fa-gears"></i> Account Settings<i class="fa fa-fw fa-caret-down"></i></a>
            <ul id="setting" class="collapse">
                <li>
                    <?php
                    echo $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']) . " Edit Profile", ['controller' => 'Users', 'action' => 'editprofile'], ['escape' => false]
                    );
                    ?>
                </li>
                <li>
                    <?php
                    echo $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-credit-card']) . " Change Password", ['controller' => 'Users', 'action' => 'changepssword'], ['escape' => false]
                    );
                    ?>
                </li>
            </ul>
        </li> 