<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $vimeoUser->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $vimeoUser->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Vimeo Users'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="vimeoUsers form large-10 medium-9 columns">
    <?= $this->Form->create($vimeoUser) ?>
    <fieldset>
        <legend><?= __('Edit Vimeo User') ?></legend>
        <?php
            echo $this->Form->input('cid');
            echo $this->Form->input('client_secret');
            echo $this->Form->input('client_access_token');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
