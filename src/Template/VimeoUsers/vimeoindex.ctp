<div class="row">
    <div class="col-md-12">
        <?php echo $this->Flash->render(); ?>
         <fieldset>
             <legend><?= __('API Credintials') ?><span class="pull-right"><?php echo $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-fw fa-file-o', 'title' => 'Add New API']) . "", ['controller' => 'vimeo_users', 'action' => 'vimeoadd'], ['escape' => false, 'class' => 'btn btn-info btn-sm']); ?></span></legend>
                <div class="table-responsive">
                    <table class="table table-bordered" id="vimeoapitable">
                        <thead>
                        <th>Client Id</th><th>Client Secret Key</th><th>Client Access Token</th><th>Action</th>
                        </thead>
                        <tbody>
                            <?php if (isset($vimeouser) && !empty($vimeouser)): ?>
                                <?php  foreach ((array)$vimeouser as $key => $value) :?>
                                  
                                    <tr>
                                        <td><?=$value->cid ?></td><td><?= $value->client_secret?></td><td><?= $value->client_access_token?></td>
                                        <td>
                                            
                                            <?php echo $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-fw fa-search', 'title' => 'View']) . "", ['controller' => 'vimeo_users', 'action' => 'vimeoview', $value->id], ['escape' => false, 'class' => 'btn btn-danger btn-xs']); ?>
                                            <?php echo $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-fw fa-edit', 'title' => 'Edit']) . "", ['controller' => 'vimeo_users', 'action' => 'vimeoedit', $value->id], ['escape' => false, 'class' => 'btn btn-success btn-xs']); ?>
                                            <?php echo $this->Form->postLink(
                          $this->Html->tag('span', '', ['class' => 'fa fa-fw fa-trash-o', 'title' => 'Delete']) . "", ['action' => 'vimeodelete', $value->id], ['confirm' => __('Are you sure you want to delete # {0}?', $value->id),'escape' => false, 'class' => 'btn btn-success btn-xs']
                    ); ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>    
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div> 
    </div>

</div>