
<?php
$flashdata = $this->request->Session()->read('errorflash');
$msg = "";
if (isset($flashdata)) {
    foreach ((array) $flashdata as $flkey => $flvalue) {
        $msg.='<div class="alert alert-warning">' . $flvalue->unique . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    }
}
echo $msg;
$this->request->Session()->delete('errorflash');
?>
<div class="row">
    <div class="col-md-12">
        
<?= $this->Form->create('vimeoadd'); ?>
                <div class="form-group">
                    <fieldset>
                        <legend><?= __('Add Vimeo User') ?></legend>
                        <?php
                        echo '<div class="col-md-6">';
                        echo $this->Form->input('cid', ['type' => 'text', 'label' => 'Client Id', 'required','class'=>'form-control']);
                        echo '</div>';
                        echo '<div class="col-md-6">';
                        echo $this->Form->input('client_access_token', ['label' => 'Client Access Token Key','required','class'=>'form-control']);
                        echo '</div>';
                        echo '<div class="col-md-12">';
                        echo $this->Form->input('client_secret', ['label' => 'Client Secret Key','required','class'=>'form-control']);
                        echo '</div>';
                        echo '<div class="pull-right col-md-3">';
                        echo $this->Form->button('<i class="fa fa-fw fa-refresh"></i>Reset', ['type' => 'reset', 'class' => 'btnmargin btnmrleft btn btn-warning pull-right', 'div' => false]);
                        echo $this->Form->button('<i class="fa fa-fw fa-floppy-o"></i>Save', ['type' => 'submit', 'class' => 'btnmargin btn btn-success pull-right', 'div' => false]);
                        echo '</div>';
                        //echo $this->Form->input('active');
                        ?>
                    </fieldset>

<?= $this->Form->end() ?>
                 
    </div>
    <div class="col-md-6">

    </div>   
</div>

