<div class="row">
    <div class="col-md-offset-11">

        <div class="dropdown">
            <button class="btn btn-warning dropdown-toggle " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <?= __('Actions') ?>
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="margin-left:-76px;">
                <li><?=
                    $this->Form->postLink(
                            $this->Html->tag('span', '', ['class' => 'fa fa-fw fa-trash-o', 'title' => 'Delete']) . "Delete API", ['action' => 'vimeodelete', $vimeoUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $vimeoUser->id), 'escape' => false]
                    )
                    ?></li>
                <li><?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-fw fa-backward', 'title' => 'Back']) . " Back to API", ['action' => 'vimeoindex'], ['escape' => false]) ?></li>
                <li><?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-fw fa-file', 'title' => 'Add New']) . "New Vimeo User", ['action' => 'vimeoadd'], ['escape' => false]) ?> </li>
                 <li><?= $this->Html->link($this->Html->tag('span', '', ['class' => 'fa fa-fw fa-check', 'title' => 'Activate']) . "Active Vimeo API", ['action' => 'activate',$vimeoUser->id], ['escape' => false]) ?> </li>
            </ul>
        </div>


    </div> 
    <div class="col-md-12">
        <fieldset>
            <legend><?= __('Vimeo API details') ?> </legend>
            <ul class="nav">
                <li>  <h3 class="text-primary"><?= __('Client Id') ?></h3><h4><?= h($vimeoUser->cid) ?></h4></li>
                <li><h3 class="text-primary"><?= __('Client Secret') ?></h3><h4><?= h($vimeoUser->client_secret) ?></h4></li>
                <li><h3 class="text-primary"><?= __('Client Access Token') ?></h3><h4><?= h($vimeoUser->client_access_token) ?></h4></li>
                 <li><h3 class="text-primary"><?= __('Sataus') ?></h3><h4><?php if(h($vimeoUser->active)==1){echo 'Active';}else{echo 'Deactive';} ?></h4></li>
            </ul>
        </fieldset>    
    </div>

</div>  