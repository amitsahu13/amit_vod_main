<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
   

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Flash');
        $this->loadComponent('Curl');
        $this->loadComponent('Api');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('ManualSession');
        $this->ManualSession->setAllowedAction(array("login", "signup","status","facebookaction"));
        $this->Api->__ApiComponent("c45fde179c34bb8f346b66520128f4d0", "00fb2c402b31f19289fed59880c05567bce8c182", "http://localhost/vodapi/webapp");
    }

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $sessiondata = $this->request->Session()->read('userdata');
        if (!$this->ManualSession->checkAllow($this->request->params['action'])) {
            $returnsessionval = $this->ManualSession->Sessioncheck();
            if ($returnsessionval[0] != 1) {
                return $this->redirect(['controller' => 'users', 'action' => 'login']);
            }
        }
        $getdata = $this->ManualSession->approval($this->request->params['action']);
        if ($getdata == 3) {
            $this->redirect(['controller' => 'Users', 'action' => 'dashboard']);
        } else if ($getdata == 0) {
            
        }
    }

}
