<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller\Component;

use Cake\Controller\Component;

/**
 * CakePHP curl
 * @author user
 */
class CurlComponent extends Component {

    public function test($num1, $num2) {
        return $num1 + $num2;
    }

    public function doComplexOperation($signupdata, $currnet_url) {
        $curl = curl_init();
        $redirect = $currnet_url;
        $qr = '';
        foreach ($signupdata as $key => $value) {
            if ($qr == "") {
                $qr = $value;
            } else {
                $qr.="|" . $value;
            }
        }
        $ky = hash_hmac('sha1', $qr, '00fb2c402b31f19289fed59880c05567bce8c182');
        $signupdata['hash_key'] = $ky;
        curl_setopt($curl, CURLOPT_URL, $redirect);
        $data = $signupdata;
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Firefox');
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_COOKIEJAR, 'cookies.txt');
        curl_setopt($curl, CURLOPT_COOKIEFILE, 'cookies.txt');
        $output = curl_exec($curl);
        return $output;
    }

    public function callCurl($url, $data) {
        //$response = "";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Firefox');
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_COOKIEJAR, 'cookies.txt');
        curl_setopt($curl, CURLOPT_COOKIEFILE, 'cookies.txt');
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
        
        
    }

}
